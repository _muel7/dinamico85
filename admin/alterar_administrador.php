<?php
$id= filter_input(INPUT_GET,'id');//filter_input recupera as informações passadas anteriormente...
$nome_adm= filter_input(INPUT_GET,'nome');
$email_adm= filter_input(INPUT_GET,'email');
$login_adm= filter_input(INPUT_GET,'login');
$senha_adm= filter_input(INPUT_GET,'senha');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Alterar adminsitrador</title>
</head>
<body>
    <form action="op_adm.php" method="get" enctype="multipart/form-data">
       <fieldset> 
            <legend> Alteração de administrador </legend>
                <header>
                    <input type="hidden" name="id" value="<?php echo $id; ?>">
                </header>

                <header> 
                    <label for="">Nome</label>
                    <input type="text" name="name" value="<?php echo $nome_adm; ?>">
                </header>

                <header> 
                    <label for="">Email</label>
                    <input type="text" name="email" value="<?php echo $email_adm; ?>">
                </header>

                <header> 
                    <label for="">Login</label>
                    <input type="text" name="login" value="<?php echo $login_adm; ?>">
                </header>
                
                <header> 
                    <label for="">Senha</label>
                    <input type="text" name="senha" value="<?php echo md5($senha_adm); ?>">
                </header>

                <header>
                    <input type="submit" name="alterar" value="Registrar alteração?">
                </header>
        </fieldset>
    </form>
</body>
</html>