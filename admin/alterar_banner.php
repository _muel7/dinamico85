<?php
$id_banner= filter_input(INPUT_GET,'id_banner');//filter_input recupera as informações passadas anteriormente...
$titulo_banner= filter_input(INPUT_GET,'titulo_banner');
$link_banner= filter_input(INPUT_GET,'link_banner');
$img_banner= filter_input(INPUT_GET,'img_banner');
$alt_banner= filter_input(INPUT_GET,'alt');
$banner_ativo= filter_input(INPUT_GET,'banner_ativo');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Alterar Banner</title>
</head>
<body>
    <form action="op_banner.php" method="post" enctype="multipart/form-data">
       <fieldset> 
            <legend> Alteração do Banner </legend>
                <header>
                    <input type="hidden" name="id_banner" value="<?php echo $id_banner; ?>">
                </header>

                <header> 
                    <label for="">Título Banner</label>
                    <input type="text" name="titulo_banner" value="<?php echo $titulo_banner; ?>">
                </header>

                <header>
                    <label for="">Link Banner</label>
                    <input type="url" name="link_banner" value="<?php echo $link_banner; ?>">
                </header>

                <header>
                    <label for="">Imagem Banner</label>
                    <input type="file" name="img_banner" value="<?php echo $img_banner;?>"> 
                </header>

                <header>
                    <label for="">Alt Banner </label>
                    <input type="text" name="alt" value="<?php echo $alt_banner?>">
                </header>

                <header> 
                    <label for="">Banner ativo</label>
                    <input type="checkbox" name="banner_ativo"<?php echo $banner_ativo==1?'checked':''; ?>>
                </header>

                <header>
                    <input type="submit" name="alterar" value="Registrar alteração?">
                </header>
        </fieldset>
    </form>
</body>
</html>