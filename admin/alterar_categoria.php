<?php
$id_categoria= filter_input(INPUT_GET,'id_categoria');//filter_input recupera as informações passadas anteriormente...
$categoria= filter_input(INPUT_GET,'categoria');
$cat_ativo= filter_input(INPUT_GET,'cat_ativo');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Alterar Categoria</title>
</head>
<body>
    <form action="op_categoria.php" method="post" enctype="multipart/form-data">
       <fieldset> 
            <legend> Alteração da Categoria </legend>
                <header>
                    <input type="hidden" name="id_categoria" value="<?php echo $id_categoria; ?>">
                </header>

                <header> 
                    <label for="">Categoria</label>
                    <input type="text" name="categoria" value="<?php echo $categoria; ?>">
                </header>

                <header> 
                    <label for="">Categoria ativa/inativa</label>
                    <input type="checkbox" name="cat_ativo"<?php echo $cat_ativo==1?'checked':''; ?>>
                </header>

                <header>
                    <input type="submit" name="alterar" value="Registrar Alteração">
                </header>
        </fieldset>
    </form>
</body>
</html>