<?php
$id_noticia= filter_input(INPUT_GET,'id_noticia');// O filter input recupera as informações passadas anteriormente.
$id_categoria= filter_input(INPUT_GET,'id_categoria');
$titulo_noticia= filter_input(INPUT_GET,'titulo_noticia');
$visita_noticia= filter_input(INPUT_GET,'visita_noticia');
$date_noticia= filter_input(INPUT_GET,'data_noticia');
$img_noticia= filter_input(INPUT_GET,'img_noticia');
$noticia= filter_input(INPUT_GET,'noticia');
$noticia_ativo= filter_input(INPUT_GET,'noticia_ativo');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Alterar Noticias</title>
</head>
<body>
    <form action="op_noticia.php" method="post" enctype="multipart/form-data">
        <fieldset>
            <legend> Alterar Noticia </legend>
                <header>
                    <input type="hidden" name="id_noticia" value="<?php echo $id_not; ?>">
                </header>

                <header>
                    <input type="hidden" name="id_categoria" value="<?php echo $id_categoria;?>">
                </header>

                <header>
                    <label for=""> Titulo Notícia</label>
                    <input type="text" name="titulo" value="<?php echo $titulo_noticia; ?>">
                </header>

                <header>
                    <label for=""> Visita Notícia</label>
                    <input type="number" name="visita" value="<?php echo $visita_noticia; ?>">
                </header>

                <header>
                    <label for=""> Data Notícia</label>
                    <input type="date" name="date" value="<?php echo $date_noticia; ?>">
                </header>

                <header>
                    <label for="">Imagem de Notícia</label>
                    <input type="image" name="imagem" value="<?php echo $img_noticia; ?>">
                </header>

                <header>
                    <label for="">Notícia</label>
                    <input type="text" name="noticia" value="<?php echo $noticia; ?>">
                </header>

                <header>
                    <label for="">Notícia ativa</label>
                    <input type="checkbox" name="noticia_ativo" value="<?php echo $noticia_ativo==1?'checked':''; ?>">
                </header>

                <header>
                    <input type="submit" name="alterar" value="Registrar alteração?">
                </header>
        </fieldset>
    </form>    
</body>
</html>