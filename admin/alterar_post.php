<?php 
$id_post= filter_input(INPUT_GET,'id_post');
$id_categoria= filter_input(INPUT_GET,'id_categoria');
$titulo_post= filter_input(INPUT_GET,'titulo_post');
$descricao_post= filter_input(INPUT_GET,'descricao_post');
$img_post= filter_input(INPUT_GET,'img_post');
$visita_post= filter_input(INPUT_GET,'visitas');
$data_post= filter_input(INPUT_GET,'data_post');
$post_ativo= filter_input(INPUT_GET,'post_ativo');
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Alterar Post</title>
</head>
<body>
    <form action="op_post.php" method="post" enctype="multipart/form-data">
        <fieldset>
            <legend>Alterar Post</legend>
            <header>
                <input type="hidden" name="id_post" value="<?php echo $id_post;?>">
            </header>
            
            <header>
                <input type="hidden" name="id_categoria" value="<?php echo $id_categoria;?>">
            </header>

            <header>
                <label for="">Título Post</label>
                <input type="text" name="titulo_post" value="<?php $titulo_post;?>">
            </header>

            <header>
                <label for="">Descrição Post</label>
                <input type="text" name="descricao_post" value="<?php $descricao_post;?>">
            </header>

            <header>
                <label for="">Imagem Post</label>
                <input type="image" name="img_post" value="<?php $img_post;?>">
            </header>

            <header>
                <label for="">Visitas Post</label>
                <input type="number" name="visita" value="<?php $visita_post;?>">
            </header>

            <header>
                <label for="">Data Post</label>
                <input type="date" name="data_post" value="<?php $data_post;?>">
            </header>

            <header>
                <label for="">Post ativo</label>
                <input type="checkbox" name="post_ativo" value="<?php $post_ativo==1?'checked':''; ;?>">
            </header>

            <header>
                <input type="submit" name="alterar" value="Registrar alteração?">
            </header>
        </fieldset>
    </form>
</body>
</html>