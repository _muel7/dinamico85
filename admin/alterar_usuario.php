<?php
$id_usuario= filter_input(INPUT_GET,'id_usuario');//filter_input recupera as informações passadas anteriormente...
$nome_usuario= filter_input(INPUT_GET,'nome_usuario');
$email_usuario= filter_input(INPUT_GET,'email_usuario');
$senha_usuario= filter_input(INPUT_GET,'senha_usuario');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Alterar Usuario</title>
</head>
<body>
    <form action="op_usuario.php" method="post" enctype="multipart/form-data">
       <fieldset> 
            <legend> Alteração de Usuario </legend>
                <header>
                    <input type="hidden" name="id_usuario" value="<?php echo $id_usuario; ?>">
                </header>

                <header> 
                    <label for="">Nome</label>
                    <input type="text" name="name_usuario" value="<?php echo $nome_usuario; ?>">
                </header>
                <br>
                <header> 
                    <label for="">Email</label>
                    <input type="text" name="email_usuario" value="<?php echo $email_usuario; ?>">
                </header>
                <br>
                <header> 
                    <label for="">Senha</label>
                    <input type="text" name="senha_usuario" value="<?php echo $senha_usuario; ?>">
                </header>
                <br>
                <header>
                    <input type="submit" name="alterar" value="Registrar alteração?">
                </header>
        </fieldset>
    </form>
</body>
</html>