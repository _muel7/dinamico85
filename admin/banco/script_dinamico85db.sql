-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema dinamico85db
-- --------------------------------------------------

-- ---------------------------------------------------
-- Schema dinamico85db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `dinamico85db` DEFAULT CHARACTER SET utf8mb4 ;
USE `dinamico85db` ;

-- -----------------------------------------------------
-- Table `dinamico85db`.`administrador`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dinamico85db`.`administrador` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(200) NOT NULL,
  `email` VARCHAR(200) NOT NULL,
  `login` VARCHAR(100) NOT NULL,
  `senha` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `dinamico85db`.`banner`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dinamico85db`.`banner` (
  `id_banner` INT(11) NOT NULL AUTO_INCREMENT,
  `titulo_banner` VARCHAR(255) NOT NULL,
  `link_banner` VARCHAR(255) NOT NULL,
  `img_banner` VARCHAR(150) NOT NULL,
  `alt` VARCHAR(255) NOT NULL,
  `banner_ativo` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`id_banner`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `dinamico85db`.`categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dinamico85db`.`categoria` (
  `id_categoria` INT(11) NOT NULL AUTO_INCREMENT,
  `categoria` VARCHAR(150) NOT NULL,
  `cat_ativo` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`id_categoria`))
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `dinamico85db`.`noticias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dinamico85db`.`noticias` (
  `id_noticia` INT(11) NOT NULL AUTO_INCREMENT,
  `id_categoria` INT(11) NOT NULL,
  `titulo_noticia` VARCHAR(255) NOT NULL,
  `img_noticia` VARCHAR(100) NOT NULL,
  `visita_noticia` INT(11) NOT NULL,
  `data_noticia` DATE NOT NULL,
  `noticia_ativo` VARCHAR(1) NOT NULL,
  `noticia` TEXT NOT NULL,
  PRIMARY KEY (`id_noticia`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `dinamico85db`.`post`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dinamico85db`.`post` (
  `id_post` INT(11) NOT NULL AUTO_INCREMENT,
  `id_categoria` INT(11) NOT NULL,
  `titulo_post` VARCHAR(250) NOT NULL,
  `descricao_post` TEXT NOT NULL,
  `img_post` VARCHAR(200) NOT NULL,
  `visitas` INT(11) NOT NULL,
  `data_post` DATE NOT NULL,
  `post_ativo` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`id_post`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `dinamico85db`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dinamico85db`.`usuario` (
  `id_usuario` INT(11) NOT NULL AUTO_INCREMENT,
  `nome_usuario` VARCHAR(255) NOT NULL,
  `email_usuario` VARCHAR(255) NOT NULL,
  `senha_usuario` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id_usuario`))
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = latin1;

USE `dinamico85db` ;

-- -----------------------------------------------------
-- procedure sp_adm_insert
-- -----------------------------------------------------

DELIMITER $$
USE `dinamico85db`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_adm_insert`(_nome varchar(255),_email varchar(200),_login varchar(100),_senha varchar(100))
begin
	insert into administrador (nome,email,login,senha) values (_nome,_email,_login,_senha);
    select * from administrador where id = LAST_INSERT_ID();
end$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_banner_insert
-- -----------------------------------------------------

DELIMITER $$
USE `dinamico85db`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_banner_insert`(_titulo varchar(255),_link varchar(255),_img varchar(255),alt varchar(255),_ativo varchar(1))
begin
      insert into banner (titulo_banner,link_banner,img_banner,alt,banner_ativo) values (_titulo, _link, _img, alt, _ativo);
    select * from banner where id_banner = LAST_INSERT_ID();
end$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_categoria_insert
-- -----------------------------------------------------

DELIMITER $$
USE `dinamico85db`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_categoria_insert`(categoria varchar(255), ativo varchar(1))
begin
  insert into categoria (categoria, cat_ativo) values (categoria, ativo);
  select * from categoria where id_categoria = LAST_INSERT_ID();
end$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_noticias_insert
-- -----------------------------------------------------

DELIMITER $$
USE `dinamico85db`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_noticias_insert`(_id_categoria int(11),_titulo varchar(255),_img varchar(255), _visita int(11), _data_noticia date, _ativo varchar(1), _noticia text)
begin
  insert into noticias (id_categoria,titulo_noticia,img_noticia,visita_noticia,data_noticia,noticia_ativo,noticia) values (_id_categoria,_titulo,_img,_visita,_data_noticia,_ativo,_noticia);
  select * from noticias where id_noticia = LAST_INSERT_ID();
end$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_post_insert
-- -----------------------------------------------------

DELIMITER $$
USE `dinamico85db`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_post_insert`(_id_categoria int(11),_titulo varchar(255),_descricao varchar(255),_img varchar(255),_visita int(11),_data_post date,_ativo varchar(1))
begin
  insert into post (id_categoria,titulo_post,descricao_post,img_post,visitas,data_post,post_ativo) values (_id_categoria,_titulo,_descricao,_img,_visita,_data_post,_ativo);
  select * from post where id_post = LAST_INSERT_ID();
end$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure sp_usuario_insert
-- -----------------------------------------------------

DELIMITER $$
USE `dinamico85db`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_usuario_insert`(_nome varchar(255),_email varchar(255), _senha varchar(255))
begin
  insert into usuario (nome_usuario, email_usuario, senha_usuario) values (_nome,_email,_senha);
  select * from usuario where id_usuario = LAST_INSERT_ID();
end$$

DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
