<?php
    //Declaração de atributos;
    class Banner{
        private $id;
        private $titulo_banner;
        private $link_banner;
        private $img_banner;
        private $alt;
        private $banner_ativo;

        //DECLARANDO Valores aos atributos;
        public function getId_banner(){
            return $this->id_banner;
        }

        public function setId_banner($value){
            $this->id = $value;
        }
        
        public function getTitulo_banner(){
            return $this->titulo_banner;
        }
        
        public function setTitulo_banner($value){
            $this->titulo_banner = $value;
        }

        public function getLink_banner(){
            return $this->link_banner;
        }
        
        public function setLink_banner($value){
            $this->link_banner = $value;
        }

        public function getImg_banner(){
            return $this->img_banner;
        }
        
        public function setImg_banner($value){
            $this->img_banner = $value;
        }

        public function getBanner_ativo(){
            return $this->banner_ativo;
        }
        
        public function setBanner_ativo($value){
            $this->banner_ativo = $value;
        }

        public function getAlt(){
            return $this->alt;
        }

        public function setAlt($value){
            $this->alt = $value;
        }

            
        //Método construtivo;
        public function loadById($_id){
            $sql = new Sql();
            $results = $sql->select("SELECT * FROM banner WHERE id = :id", array(':id'=>$_id));
            if(count($results)>0){
                $this->setData($results[0]);
            }
        }
        public static function getList(){
            $sql = new Sql();
            return $sql->select("SELECT * FROM banner order by id");
        }

        public static function search($titulo_banner){
            $sql = new Sql(); 
            return $sql->select("SELECT * FROM banner WHERE titulo_banner LIKE:titulo_banner",array(":titulo_banner"=>"%".$titulo_banner."%"));
        }

        public function setData($dados){
            $this->setId_banner($dados['id_banner']);
            $this->setTitulo_banner($dados['titulo_banner']);
            $this->setLink_banner($dados['link_banner']);
            $this->setImg_banner($dados['img_banner']);
            $this->setAlt($dados['alt']);
            $this->setBanner_ativo($dados['banner_ativo']);
        }

        public function insert(){
            $sql = new Sql();
            $results = $sql->select("CALL sp_banner_insert(:titulo_banner, :link_banner, :img_banner, :alt, :banner_ativo)", 
            array(
                ":titulo_banner"=>$this->getTitulo_banner(),
                ":link_banner"=>$this->getLink_banner(),
                ":img_banner"=>$this->getImg_banner(),
                ":alt"=>$this->getAlt(),
                ":banner_ativo"=>$this->getBanner_ativo()
            ));

            if(count($results)>0){
            $this->setData($results[0]);
            }
        }

        public function update($titulo_banner, $link_banner, $img_banner, $alt, $banner_ativo){
            $sql = new Sql();
            $sql->query("UPDATE banner SET titulo_banner = :titulo_banner, link_banner = :link_banner, img_banner = :img_banner, alt = :alt, banner_ativo = :banner_ativo)", 
            array(
                ":titulo_banner"=>$titulo_banner,
                ":link_banner"=>$link_banner,
                ":img_banner"=>$img_banner,
                ":alt"=>$alt,
                ":banner_ativo"=>$banner_ativo
                
            ));
        }

        public function delete(){
            $sql = new Sql();
            $sql->query("DELETE FROM banner WHERE id_banner = :id_banner",array(":id_banner"=>$this->getId_banner()));
        }

        public function __construct($id="", $titulo_banner="", $link_banner="", $img_banner="", $alt="", $banner_ativo=""){
            $this->id = $id;
            $this->titulo_banner = $titulo_banner;
            $this->link_banner = $link_banner;
            $this->img_banner = $img_banner;
            $this->alt = $alt;
            $this->banner_ativo = $banner_ativo;
        }
    }
?>