<?php
    //Declaração de atributos;
    class Categoria{
        private $id_categoria;
        private $categoria;
        private $cat_ativo;
      

        //DECLARANDO Valores aos atributos;
        public function getIdCategoria(){
            return $this->id_categoria;
        }

        public function setIdCategoria($value){
            $this->id_categoria = $value;
        }
        
        public function getCategoria(){
            return $this->categoria;
        }
        
        public function setCategoria($value){
            $this->categoria = $value;
        }

        public function getCat_ativo(){
            return $this->cat_ativo;
        }

        public function setCat_ativo($values){
            $this->cat_ativo = $values;
        }
            
        //Método construtivo;
        public function loadById($id_categoria){
            $sql = new Sql();
            $results = $sql->select("SELECT * FROM categoria WHERE id_categoria = :id_categoria", array(':id_categoria'=>$id_categoria));
            if(count($results)>0){
                $this->setData($results[0]);
            }
        }
        public static function getList(){
            $sql = new Sql();
            return $sql->select("SELECT * FROM categoria order by categoria");
        }

        public static function search($categoria){
            $sql = new Sql(); 
            return $sql->select("SELECT * FROM categoria WHERE categoria LIKE :categoria",array(":categoria"=>"%".$categoria."%"));
        }

        public function setData($dados){
            $this->setIdCategoria($dados['id_categoria']);
            $this->setCategoria($dados['categoria']);
            $this->setCat_ativo($dados['cat_ativo']);
        }

        public function insert(){
            $sql = new Sql();
            $results = $sql->select("CALL sp_categoria_insert(:categoria, :cat_ativo)", 
            array(
                ":categoria"=>$this->getCategoria(),
                ":cat_ativo"=>$this->getCat_ativo(),
            ));

            if(count($results)>0){
            $this->setData($results[0]);
            }
        }

        public function update($id_categoria, $categoria, $cat_ativo){
            $sql = new Sql();
            $sql->query("UPDATE categoria SET categoria = :categoria, cat_ativo = :cat_ativo WHERE id_categoria = :id_categoria", 
            array(
                ":id_categoria"=>$id_categoria,
                ":categoria"=>$categoria,
                ":cat_ativo"=>$cat_ativo
            ));
        }

        public function delete(){
            $sql = new Sql();
            $sql->query("DELETE FROM categoria WHERE id_categoria = :id_categoria",array(":id_categoria"=>$this->getIdCategoria()));
        }
        public function __construct($id_categoria="", $categoria="", $cat_ativo=""){
            $this->id_categoria = $id_categoria;
            $this->categoria = $categoria;
            $this->cat_ativo = $cat_ativo;
        }
    }
?>