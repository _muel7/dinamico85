<?php
    //Declaração de atributos;
    class Noticia{
        private $id;
        private $id_categoria;
        private $titulo;
        private $imagem;
        private $visita;
        private $date;
        private $noticia_ativo;
        private $noticia;

        //DECLARANDO Valores aos atributos;
        public function getIdNoticia(){
            return $this->id;
        }

        public function setIdNoticia($value){
            $this->id = $value;
        }
        
        public function getId_Categoria(){
            return $this->id_categoria;
        }
        
        public function setId_Categoria($value){
            $this->id_categoria = $value;
        }

        public function getTitulo(){
            return $this->titulo;
        }
        
        public function setTitulo($value){
            $this->titulo = $value;
        }

        public function getImagem(){
            return $this->imagem;
        }
        
        public function setImagem($value){
            $this->imagem = $value;
        }

        public function getVisita(){
            return $this->visita;
        }
        
        public function setVisita($value){
            $this->visita = $value;
        }

        public function getDate(){
            return $this->date;
        }

        public function setDate($value){
            $this->date = $value;
        }

        public function getNoticia_ativo(){
            return $this->noticia_ativo;
        }

        public function setNoticia_ativo($value){
            $this->noticia_ativo = $value;
        }

        public function getNoticia(){
            return $this->noticia;
        }

        public function setNoticia($value){
            $this->noticia = $value;
        }
            
        //Método construtivo;
        public function loadById($_id){
            $sql = new Sql();
            $results = $sql->select("SELECT * FROM noticias WHERE id_noticia = :id", array(':id'=>$_id));
            if(count($results)>0){
                $this->setData($results[0]);
            }
        }
        public static function getList(){
            $sql = new Sql();
            return $sql->select("SELECT * FROM noticias order by id_noticia ");
        }

        public static function search($titulo_noticia){
            $sql = new Sql(); 
            return $sql->select("SELECT * FROM noticias WHERE titulo_noticia LIKE:titulo_noticia",array(":titulo_noticia"=>"%".$titulo_noticia."%"));
        }
        
        public function updateVisita($id){
        $sql = new Sql();
        $sql->query("UPDATE noticias SET visita_noticia = visita_noticia +1 WHERE id_noticia = :id",
        array(
            ":id"=>$id,
        ));
        }

        public function setData($dados){
            $this->setIdNoticia($dados['id_noticia']);
            $this->setId_Categoria($dados['id_categoria']);
            $this->setTitulo($dados['titulo_noticia']);
            $this->setVisita($dados['visita_noticia']);
            $this->setImagem($dados['img_noticia']);
            $this->setDate($dados['data_noticia']);
            $this->setNoticia_ativo($dados['noticia_ativo']);
            $this->setNoticia($dados['noticia']);
        }

        public function insert(){
            $sql = new Sql();
            $results = $sql->select("CALL sp_noticias_insert(:id_categoria, :titulo_noticia, :img_noticia, :visita_noticia, :data, :noticia_ativo, :noticia)", 
            array(
                ":id_categoria"=>$this->getId_Categoria(),
                ":titulo_noticia"=>$this->getTitulo(),
                ":visita_noticia"=>$this->getVisita(),
                ":img_noticia"=>$this->getImagem(),
                ":data"=>$this->getDate(),
                ":noticia"=>$this->getNoticia(),
                ":noticia_ativo"=>$this->getNoticia_ativo()
            ));

            if(count($results)>0){
            $this->setData($results[0]);
            }
        }

        public function update($id_noticia, $id_categoria, $titulo_noticia, $visita, $date, $img_noticia, $noticia, $noticia_ativo){
            $sql = new Sql();
            $sql->query("UPDATE noticias SET id_noticia = id_noticia, id_categoria = :id_categoria, titulo_noticia = :titulo_noticia, visita_noticia = :visita_noticia, date = :date, img_noticia = :img_noticia, noticia = :noticia, noticia_ativo = :noticia-ativo)", 
            array(
                "id_noticia"=>$id_noticia,
                "id_categoria"=>$id_categoria,
                "titulo_noticia"=>$titulo_noticia,
                "visita_noticia"=>$visita,
                "date"=>$date,
                "img_noticia"=>$img_noticia,
                "noticia"=>$noticia,     
                "noticia_ativo"=>$noticia_ativo,           
            ));
        }

        public function delete(){
            $sql = new Sql();
            $sql->query("DELETE FROM noticias WHERE id_noticia = :id_noticia",array(":id_noticia"=>$this->getIdNoticia()));
        }
        public function __construct($id="", $id_categoria="", $titulo_noticia="", $img_noticia="", $visita_noticia="", $date="", $noticia="", $noticia_ativo=""){
            $this->id = $id;
            $this->id_categoria = $id_categoria;
            $this->titulo = $titulo_noticia;
            $this->imagem = $img_noticia;
            $this->visita = $visita_noticia;
            $this->date = $date;
            $this->noticia = $noticia;
            $this->noticia_ativo = $noticia_ativo;
        }
    }
?> 