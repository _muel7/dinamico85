<?php
    //Declaração de atributos;
    class Post{
        private $id_post;
        private $titulo;
        private $id_categoria;
        private $descricao;
        private $imagem;
        private $visitas;
        private $data;
        private $post_ativo;

        //DECLARANDO Valores aos atributos;
        public function getIdPost(){
            return $this->id_post;
        }

        public function setIdPost($value){
            $this->id_post = $value;
        }
        
        public function getTitulo(){
            return $this->titulo;
        }
        
        public function setTitulo($value){
            $this->titulo = $value;
        }

        public function getId_categoria(){
            return $this->id_categoria;
        }
        
        public function setId_categoria($value){
            $this->id_categoria = $value;
        }

        public function getDescricao(){
            return $this->descricao;
        }
        
        public function setDescricao($value){
            $this->descricao = $value;
        }

        public function getImagem(){
            return $this->imagem;
        }

        public function setImagem($value){
            $this->imagem = $value;
        }

        public function getVisitas(){
            return $this->visitas;
        }
        
        public function setVisitas($value){
            $this->visitas = $value;
        }

        public function getDate(){
            return $this->data;
        }

        public function setDate($value){
            $this->data = $value;
        }

        public function getPost_ativo(){
            return $this->post_ativo;
        }

        public function setPost_ativo($value){
            $this->post_ativo = $value;
        }

            
        //Método construtivo;
        public function loadById($id_post){
            $sql = new Sql();
            $results = $sql->select("SELECT * FROM post WHERE id_post = :id_post", array(':id_post'=>$id_post));
            if(count($results)>0){
                $this->setData($results[0]);
            }
        }
        public static function getList(){
            $sql = new Sql();
            return $sql->select("SELECT * FROM post order by titulo_post");
        }

        public static function search($titulo_post){
            $sql = new Sql(); 
            return $sql->select("SELECT * FROM post WHERE titulo_post LIKE:titulo_post",array(":titulo_post"=>"%".$titulo_post."%"));
        }

        public function setData($dados){
            $this->setIdPost($dados['id_post']);
            $this->setId_categoria($dados['id_categoria']);
            $this->setTitulo($dados['titulo_post']);
            $this->setDescricao($dados['descricao_post']);
            $this->setImagem($dados['img_post']);
            $this->setVisitas($dados['visitas']);
            $this->setDate($dados['data_post']);
            $this->setPost_ativo($dados['post_ativo']);
        }

        public function updateVisita($id){
            $sql = new Sql();
            $sql->query("UPDATE post SET visitas = visitas +1 WHERE id_post = :id",
            array(
                ":id"=>$id,
            ));
            }

        public function insert(){
            $sql = new Sql();
            $results = $sql->select("CALL sp_post_insert(:_idcategoria, :_titulo_post, :descricao_post, :_img, :visitas, :data , :post_ativo)", 
            array(
                ":_idcategoria"=>$this->getId_categoria(),
                ":_titulo_post"=>$this->getTitulo(),
                ":descricao_post"=>$this->getDescricao(),
                ":_img"=>$this->getImagem(),
                ":visitas"=>$this->getVisitas(),
                ":data"=>$this->getDate(),
                ":post_ativo"=>$this->getPost_ativo(),
            ));
            if(count($results)>0){
            $this->setData($results[0]);
            }
        }

        public function update($id_post, $id_categoria, $titulo_post, $descricao, $img_post, $visitas, $post_ativo){
            $sql = new Sql();
            $sql->query("UPDATE post SET id_post = :id_post, id_categoria = :id_categoria, = :titulo_post, descricao_post = :descricao_post, img_post = :img_post, visitas = :visitas, post_ativo = :post_ativo)", 
            array(
                "id_post"=>$id_post,
                "id_categoria"=>$id_categoria,
                "titulo_post"=>$titulo_post,
                "descricao_post"=>$descricao,
                "img_post"=>$img_post,
                "visitas"=>$visitas,
                "post_ativo"=>$post_ativo                
            ));
        }

        public function delete(){
            $sql = new Sql();
            $sql->query("DELETE FROM post WHERE id_post = :id_post",array(":id_post"=>$this->getIdPost()));
        }
        public function __construct($id_post="", $id_categoria="", $titulo_post="", $descricao="", $img_post="", $visitas="", $data="", $post_ativo=""){
            $this->id_post = $id_post;
            $this->id_categoria = $id_categoria;
            $this->titulo_post = $titulo_post;
            $this->descricao = $descricao;
            $this->img_post = $img_post;
            $this->visitas = $visitas;
            $this->data = $data;
            $this->post_ativo = $post_ativo;
        }
    }
?>