<?php
//Atributos
    class Usuario{
        private $id_usuario;
        private $nome_usuario;
        private $email_usuario;
        private $senha_usuario;


        public function getIdUsuario(){
            return $this->id_usuario;
        }

        public function setIdUsuario($value){
            $this->id_usuario = $value;
        }

        public function getNomeUsuario(){
            return $this->nome_usuario;
        }

        public function setNomeUsuario($value){
            $this->nome_usuario = $value;
        }
            
        public function getEmailUsuario(){
            return $this->email_usuario;
        }

        public function setEmailUsuario($value){
            $this->email_usuario = $value;
        }

        public function getSenhaUsuario(){
            return $this->senha_usuario;
        }

        public function setSenhaUsuario($value){
            $this->senha_usuario = $value;

        }

        //Métodos construtivo;
        public function loadById($_id){
            $sql = new Sql();
            $results = $sql->select("SELECT * FROM usuario WHERE id = :id", array(':id'=>$_id));
            if(count($results)>0){
                $this->setData($results[0]);
            }
        }

        public static function getList(){
            $sql = new Sql();
            return $sql->select("SELECT * FROM usuario order by id_usuario");
        }

        public static function search($nome_usuario){
            $sql = new Sql();
            return $sql->select("SELECT * FROM usuario WHERE nome_usuario LIKE:nome_usuario",array("nome_usuario"=>"%".$nome_usuario."%"));
        }

        public function insert(){
            $sql = new Sql();
            $results = $sql->select("CALL sp_usuario_insert(:nome, :email, :senha)",
            array(
                ":nome"=>$this->getNomeUsuario(),
                ":email"=>$this->getEmailUsuario(),
                ":senha"=>md5($this->getSenhaUsuario())
            ));
            if(count($results)>0){
                $this->setData($results[0]);
            }
        }

        public function efetuarLogin($_email, $_senha){
            $sql = new Sql();
            $senha_cript = md5($_senha);
            $results = $sql->select("SELECT * FROM usuario WHERE email_usuario = :email_usuario AND senha_usuario = :senha_usuario",
            array(':email_usuario'=>$_email,":senha_usuario"=>$senha_cript));
            if (count($results)>0){
                $this->setData($results[0]);
            }
        }

        public function updateUsuario($id_usuario, $nome_usuario, $email_usuario, $senha_usuario){
            $sql = new Sql();
            $sql->query("UPDATE usuario SET id_usuario = :id_usuario, nome_usuario = :nome_usuario, email_usuario = :email_usuario, senha_usuario = :senha_usuario)",
            array(
                ":id_usuario"=>$id_usuario,
                ":nome_usuario"=>$nome_usuario,
                ":email_usuario"=>$email_usuario,
                ":senha_usuario"=>$senha_usuario
            ));
        }

        public function setData($dados){
            $this->setIdUsuario($dados['id_usuario']);
            $this->setNomeUsuario($dados['nome_usuario']);
            $this->setEmailUsuario($dados['email_usuario']);
        }

        public function delete(){
            $sql = new Sql();
            $sql->query("DELETE FROM usuario WHERE id_usuario = :id_usuario",array(":id_usuario"=>$this->getIdUsuario()));
        }

        public function __construct($id_usuario="", $nome_usuario="", $email_usuario="", $senha_usuario=""){
            $this->id_usuario = $id_usuario;
            $this->nome_usuario = $nome_usuario;
            $this->email_usuario = $email_usuario;
            $this->senha_usuario = $senha_usuario;
        }

    }
?>