<?php         
    require_once('../config.php');
    $cats = Categoria::getList();
?>

<div id="box-cadastro">
    <div id="formulario-menor">
        <form id="frmpost" enctype="multipart/form-data" name="frmpost" action="op_post.php" method="POST">
            <fieldset>
                <legend>Cadastro de Post</legend>
                    <label>
                        <span>Id categoria</span>
                        <select name="categoria" id="categoria">
                            <?php 
                                foreach($cats as $cat){
                            ?>
                            <option value="<?php echo($cat['id_categoria'])?>"><?php echo($cat['categoria'])?></option>
                            <?php }?>
                        </select>
                    <span>Título post</span>
                        <input type="text" name="txt_titulo_post" id="txt_titulo_post" value="" required>
                    </label>
                    <label>
                        <span>Descriçao post</span>
                        <input type="text" name="txt_descricao" id="txt_descricao" value="" required>
                    </label>
                    <label>
                        <span>Imagem post</span>
                        <input type="file" name="foto" id="txt_imagem" value="" required>
                    </label>
                    <label>
                        <span>Visitas</span>
                        <input type="text" name="txt_visita" id="txt_visita" value="" required>
                    </label>
                    <label>
                        <span>Data</span>
                        <input type="date" name="txt_data" id="txt_data" value="" required>
                    </label>
                    
                        <div>
                            <p id="post_ativo">Ativo <input type="checkbox" name="check_ativo" checked></p>
                        </div>
                        <br>
                        <input type="submit" value="Cadastrar" class="botao" name="btn_cadastrar">
                        <span><?php echo (isset($_GET['msg']))?"Sucesso!":'';?></span>
            </fieldset>
        </form>
    </div>
</div>