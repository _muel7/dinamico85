<?php
require_once('../config.php');
if(isset($_POST['cadastro_usuario'])){
    $nome_usuario=$_POST['nome_usuario'];
    $email_usuario=$_POST['email_usuario'];
    $foto_usuarios=$_FILES['foto_usuario'];
    $senha_usuario=$_POST['senha_usuario'];

    if(!empty($foto_usuarios['name'])){
        $largura = LARGURA_IMG;
        $altura = 425;
        $tamanho= 300000;
        $error = array();
        if (!preg_match("/^image\/()pjpeg|jpeg|png|gif|bmp)$/",$foto['type'])){
            $error[1] = "Este arquivo não é uma imagem";
        }

        $dimensoes = getimagesize($foto['tmp_name']);
        if($dimensoes[0]>$largura){
            $error[2] = "A largura(".$dimensoes[0]."pixels) é maior que a suportada (".$largura." bytes)";
        }

        $dimensoes = getimagesize($foto['tmp_name']);
        if($dimensoes[1]>$altura){
            $error[3] = "A altura(".$dimensoes[1]."pixels) é maior que a suportada (".$altura." bytes)";
        }

        if($foto['size']>$tamanho){
            $error[4] = "O tamanho da imagem(".$foto[0]."bytes) é maior que a suportada (".$foto." bytes)";
        }

        //recuperar a extensao do arquivo;
        if(count($error)==0){
            preg_match("/\.(gif|bmp|png|jpg){1}$/i",$foto['name'],$ext);
            //gerar nome de arquivo para img;
            $nome_img = md5(uniqid(time())).$ext[0];
            $caminho_img = "foto/".$nome_img;
            move_uploaded_file($foto['tmp_name'],$caminho_img);
            require_once('../config.php');
            $usuario = Usuario::getList();
            if($error==0){
                    echo $erro."<br> Usuario inserido com sucesso!";
            }
        }
            if(count($error)!=0){
            foreach ($error as $erro){
                echo $erro."<br>";
            }
        }     
    } 
}
?>
    <?php
    require_once('../config.php');
    $usuario = Usuario::getList();
    ?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Cadastro Usuário</title>
</head>
<body>
    <h1>Novo Usuário</h1>
    <form action="op_usuario.php" method="post" enctype="multipart/form-data" name="cadastro_form">
        Nome:<br>
        <input type="text" name="nome" id="nome_usuario" required><br>
        Email:<br>
        <input type="text" name="email" id="email_usuario" required><br>
        Senha:<br>
        <input type="password" name="senha" id="senha_usuario" required><br>
        <br>
        <input type="submit" name="cadastro_usuario" value="Cadastrar usuario" class="btn-cadastrar">
        <span><?php echo (isset($_GET['msg']))?"Sucesso!":'';?></span>
    </form>
    <br>

</body>
</html>