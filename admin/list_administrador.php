<?php
require_once('../config.php');
$adm_retornados = Administrador::getList();
if(count($adm_retornados)>0){
?>


<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Lista Administradores</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <table id="tb_adm" width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#fff">
        <tr bgcolor="#993300" align="center">
        <th width="15%" height="2"><font size="2" color="#000">Id</font></th>
            <th width="15%" height="2"><font size="2" color="#000">Nome</font></th>
            <th width="15%" height="2"><font size="2" color="#000">Email</font></th>
            <th width="15%" height="2"><font size="2" color="#000">Login</font></th>
            <th width="15%" height="2"><font size="2" color="#000">Senha</font></th>
            <th colspan="2"><font size="2" color="#fff">Opções</font></th>
        </tr>    
        <?php
            foreach($adm_retornados as $adm){
        ?>
        <tr>
            <td <font size="2" face="verdana, arial" color="#fff">
                    <?php echo $adm['id'];?></font></td>
                <td <font size="2" face="verdana, arial" color="#fff">
                    <?php echo $adm['nome'];?></font></td>
                <td <font size="2" face="verdana, arial" color="#fff">
                    <?php echo $adm['email'];?></font></td>
                    <td <font size="2" face="verdana, arial" color="#fff">
                    <?php echo $adm['login'];?></font></td>
                    <td <font size="2" face="verdana, arial" color="#fff">
                    <?php echo $adm['senha'];?></font></td>
                <td align="center" <font size="2" face="verdana, arial" color="#fffff">
                    <a href="<?php echo"alterar_administrador.php?id=".$adm['id']."&nome=".$adm['nome']."&email=".$adm['email']."&login=".$adm['login'];?>=">Alterar</a></font></td>
                <td align="center" <font size="2" face="verdana, arial" color="#fffff">
                    <a href="<?php echo "op_adm.php?excluir=1&id=".$adm['id'];?>">Excluir</a></font>
            </td>
        </tr>
        <?php }}?>
    </table>
</body>
</html>