<?php
require_once('conexao.php');
$query = "select * from banner";
$cmd = $cn->prepare($query);
$cmd->execute();
$banners_retornados =  $cmd->fetchAll(PDO::FETCH_ASSOC);
if(count($banners_retornados)>0){

?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Lista Banner</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <table id="tb_banner" width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#fff">
        <tr bgcolor="#993300" align="center">
            <th width="15%" height="2"><font size="2" color="#000">Id</font></th>
            <th width="15%" height="2"><font size="2" color="#000">Título</font></th>
            <th width="15%" height="2"><font size="2" color="#000">Link</font></th>
            <th width="15%" height="2"><font size="2" color="#000">Imagem</font></th>
            <th width="15%" height="2"><font size="2" color="#000">Alternate</font></th>
            <th width="15%" height="2"><font size="2" color="#000">Ativo</font></th>
            <th colspan="2"><font size="2" color="#fff">Opções</font></th>
        </tr>
        <?php
            foreach($banners_retornados as $banner){ 
        ?>
        <tr>
                <td <font size="2" face="verdana, arial" color="#fff"><?php echo $banner['id_banner'];?></font></td>
                <td <font size="2" face="verdana, arial" color="#fff"><?php echo $banner['titulo_banner'];?></font></td>
                <td <font size="2" face="verdana, arial" color="#fff"><?php echo $banner['link_banner'];?></font></td>
                <td <font size="2" face="verdana, arial" color="#fff"><?php echo $banner['img_banner'];?></font></td>
                <td <font size="2" face="verdana, arial" color="#fff"><?php echo $banner['alt'];?></font></td>
                <td <font size="2" face="verdana, arial" color="#fff"><?php echo $banner['banner_ativo']=='1'?'Sim':'Não';?></font></td>
                <td align="center" <font size="2" face="verdana, arial" color="#fffff"><a href="alterar_banner.php?alterar=1&id_banner=<?php echo $banner['id_banner'];?>
                &titulo_banner=<?php echo $banner['titulo_banner'];?>
                &link_banner=<?php echo $banner['link_banner'];?>
                &img_banner=<?php echo $banner['img_banner'];?>
                &alt=<?php echo $banner['alt'];?>
                &banner_ativo=<?php echo $banner['banner_ativo'];?>">Alterar</a></font></td>
                <td align="center" <font size="2" face="verdana, arial" color="#fffff"><a href="op_banner.php?link=">Excluir</a></font></td>
            </tr>
            <?php }}?>
    </table>
</body>
</html>