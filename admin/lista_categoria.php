<?php
require_once('../config.php');
$categorias_retornadas = Categoria::getList();
if(count($categorias_retornadas)>0){
    // print_r($categorias_retornadas);
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title>Lista Categoria</title>
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <table id="tb_categoria" width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#fff">
            <tr bgcolor="#993300" align="center">
                <th width="15%" height="2"><font size="2" color="#000">Id Categoria</font></th>
                <th width="60%" height="2"><font size="2" color="#000">Categoria</font></th>
                <th width="15%" height="2"><font size="2" color="#000">Ativo</font></th>
                <th colspan="2"><font size="2" color="#fff">Opções</font></th>
            </tr>
            <?php
                foreach($categorias_retornadas as $categoria){
            ?>
            <tr>
                <td <font size="2" face="verdana, arial" color="#fff"><?php echo $categoria['id_categoria'];?></font></td>
                <td <font size="2" face="verdana, arial" color="#fff"><?php echo $categoria['categoria'];?></font></td>
                <td <font size="2" face="verdana, arial" color="#fff"><?php echo $categoria['cat_ativo']=='1'?'Sim':'Não';?></font></td>
                <td align="center" <font size="2" face="verdana, arial" color="#fffff"><a href="alterar_categoria.php?alterar=1&id_categoria=<?php echo $categoria['id_categoria'];?>&categoria=<?php echo $categoria['categoria'];?>&cat_ativo=<?php echo $categoria['cat_ativo'];?>">Alterar</a></font></td>
                <td align="center" <font size="2" face="verdana, arial" color="#fffff"><a href="op_categoria.php?excluir=1&id_categoria=<?php echo $categoria['id_categoria'];?>">Excluir</a></font></td>
            </tr>
                <?php }}?>
    </table>
</body>
</html>