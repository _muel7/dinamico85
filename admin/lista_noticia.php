<?php
require_once('../config.php');
$noticias_retornadas = Noticia::getList();
if(count($noticias_retornadas)>0){
?>


<!DOCTYPE html>
<html lang="pt-br">
<head>
   <title>Lista de Noticias</title>
   <link rel="stylesheet" href="css/style.css"> 
</head>
<body>
    <table id="tb_noticia" width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#fff">
        <tr bgcolor="#993300" align="center">
            <th width="15%" height="2"><font size="2" color="#000">Id noticia</font></th>
            <th width="15%" height="2"><font size="2" color="#000">Id categoria</font></th>
            <th width="15%" height="2"><font size="2" color="#000">Titulo</font></th>
            <th width="15%" height="2"><font size="2" color="#000">Imagem</font></th>
            <th width="15%" height="2"><font size="2" color="#000">Visita</font></th>
            <th width="15%" height="2"><font size="2" color="#000">Noticia</font></th>
            <th width="15%" height="2"><font size="2" color="#000">Data</font></th>
            <th width="15%" height="2"><font size="2" color="#000">Noticia ativo</font></th>
            <th colspan="2"><font size="2" color="#fff">Opções</font></th>
        </tr>
        <?php
        foreach($noticias_retornadas as $noticia){
        ?>
        <tr>
            <td <font size="2" face="verdana, arial" color="#fff">
                <?php echo $noticia['id_noticia'];?></font></td>
                <td <font size="2" face="verdana, arial" color="#fff">
                <?php echo $noticia['id_categoria'];?></font></td>
                <td <font size="2" face="verdana, arial" color="#fff">
                <?php echo $noticia['titulo_noticia'];?></font></td>
                <td <font size="2" face="verdana, arial" color="#fff">
                <img src="foto/<?php echo $noticia['img_noticia'];?>" alt="">
                </font></td>
                <td <font size="2" face="verdana, arial" color="#fff">
                <?php echo $noticia['visita_noticia'];?></font></td>
                <td <font size="2" face="verdana, arial" color="#fff">
                <?php echo $noticia['noticia'];?></font></td>
                <td <font size="2" face="verdana, arial" color="#fff">
                <?php echo $noticia['data_noticia'];?></font></td>
                <td <font size="2" face="verdana, arial" color="#fff">
                <?php echo $noticia['noticia_ativo'];?></font></td>             
            <td align="center" <font size="2" face="verdana, arial" color="#fffff">
                    <a href="<?php echo "alterar_noticia.php?
                    id=".$noticia['id_noticia'].
                    "&id_categoria=".$noticia['id_categoria'].
                    "&titulo=".$noticia['titulo_noticia'].
                    "&imagem=".$noticia['img_noticia'].
                    "&visita=".$noticia['visita_noticia'].
                    "&data=".$noticia['data_noticia'].
                    "&noticia_ativo=".$noticia['noticia_ativo'].
                    "&noticia=".$noticia['noticia'];?>=">Alterar</a></font></td>
            <td align="center" <font size="2" face="verdana, arial" color="#fffff">
                    <a href="<?php echo "op_noticia.php?excluir=1&id=".$noticia['id_noticia'];?>">Excluir</a></font>
        </tr>
    </table>
        <?php }}?>
</body>
</html>