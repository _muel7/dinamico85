<?php
require_once('../config.php');
$post_retornados = Post::getList();
if(count($post_retornados)>0){
?>


<!DOCTYPE html>
<html lang="pt-br">
<head>
   <title>Lista de Posts</title>
   <link rel="stylesheet" href="css/style.css"> 
</head>
<body>
    <table id="tb_post" width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#fff">
        <tr bgcolor="#993300" align="center">
            <th width="15%" height="2"><font size="2" color="#000">Id Post</font></th>
            <th width="15%" height="2"><font size="2" color="#000">Id categoria</font></th>
            <th width="15%" height="2"><font size="2" color="#000">Titulo</font></th>
            <th width="15%" height="2"><font size="2" color="#000">Descrição</font></th>
            <th width="15%" height="2"><font size="2" color="#000">Imagem</font></th>
            <th width="15%" height="2"><font size="2" color="#000">Visita</font></th>
            <th width="15%" height="2"><font size="2" color="#000">Data</font></th>
            <th width="15%" height="2"><font size="2" color="#000">Post ativo</font></th>
            <th colspan="2"><font size="2" color="#fff">Opções</font></th>
        </tr>
        <?php
        foreach($post_retornados as $post){
        ?>
        <tr>
            <td <font size="2" face="verdana, arial" color="#fff">
                <?php echo $post['id_post'];?></font></td>
                <td <font size="2" face="verdana, arial" color="#fff">
                <?php echo $post['id_categoria'];?></font></td>
                <td <font size="2" face="verdana, arial" color="#fff">
                <?php echo $post['titulo_post'];?></font></td>
                <td <font size="2" face="verdana, arial" color="#fff">
                <?php echo $post['descricao_post'];?></font></td>
                <td <font size="2" face="verdana, arial" color="#fff">
                <img src="foto/<?php echo $post['img_post'];?>" width="80" height="80" alt=""></font></td>
                <td <font size="2" face="verdana, arial" color="#fff">
                <?php echo $post['visitas'];?></font></td>
                <td <font size="2" face="verdana, arial" color="#fff">
                <?php echo $post['data_post'];?></font></td>
                <td <font size="2" face="verdana, arial" color="#fff">
                <?php echo $post['post_ativo'];?></font></td>
                                
            <td align="center" <font size="2" face="verdana, arial" color="#fffff">
                    <a href="<?php echo "alterar_post.php?id_post=".
                    $post['id_post'].
                    "&id_categoria=".
                    $post['id_categoria'].
                    "&titulo_post=".
                    $post['titulo_post'].
                    "&img_post=".
                    $post['img_post'].
                    "&visitas=".
                    $post['visitas'].
                    "&data_post=".
                    $post['data_post'].
                    "&post_ativo=".
                    $post['post_ativo'].
                    "&descricao_post=".
                    $post['descricao_post'];?>=">Alterar</a></font></td>
            <td align="center" <font size="2" face="verdana, arial" color="#fffff">
                    <a href="<?php echo "op_post.php?excluir=1&id_post=".$post['id_post'];?>">Excluir</a></font>
        </tr>
    </table>
        <?php }}?>
</body>
</html>