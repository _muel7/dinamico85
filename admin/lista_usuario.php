<?php
require_once('../config.php');
$usuarios_retornados = Usuario::getList();
if(count($usuarios_retornados)>0){
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
   <title>Lista de Usuarios</title>
   <link rel="stylesheet" href="css/style.css"> 
</head>
<body>
    <table id="tb_usuario" width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#fff">
        <tr bgcolor="#993300" align="center">
            <th width="15%" height="2"><font size="2" color="#000">Id Usuario</font></th>
            <th width="15%" height="2"><font size="2" color="#000">Nome</font></th>
            <th width="15%" height="2"><font size="2" color="#000">Email</font></th>
            <th width="15%" height="2"><font size="2" color="#000">Senha</font></th>
            <th colspan="2"><font size="2" color="#fff">Opções</font></th>
        </tr>
        <?php
        foreach($usuarios_retornados as $usuarios){
        ?>
        <tr>
            <td <font size="2" face="verdana, arial" color="#fff">
                <?php echo $usuarios['id_usuario'];?></font></td>
                <td <font size="2" face="verdana, arial" color="#fff">
                <?php echo $usuarios['nome_usuario'];?></font></td>
                <td <font size="2" face="verdana, arial" color="#fff">
                <?php echo $usuarios['email_usuario'];?></font></td>
                <td <font size="2" face="verdana, arial" color="#fff">
                <?php echo $usuarios['senha_usuario'];?></font></td>
            <td align="center" <font size="2" face="verdana, arial" color="#fffff">
                    <a href="<?php echo"alterar_usuario.php?id=".
                    $usuarios['id_usuario'].
                    "&nome=".
                    $usuarios['nome_usuario'].
                    "&email=".
                    $usuarios['email_usuario'].
                    "&senha=".
                    $usuarios['senha_usuario'];?>">Alterar</a></font></td>
            <td align="center" <font size="2" face="verdana, arial" color="#fffff">
            <a href="<?php echo "op_usuario.php?excluir=1&id=".$usuarios['id_usuario'];?>">Excluir</a></font>
        </tr>
    </table>
        <?php }}?>
</body>
</html>