<?php
    require_once('../config.php');
    $adm = new Administrador();
    if(isset($_POST['btn_cadastrar'])){     
        $adm->setNome($_POST['txt_nome_adm']);
        $adm->setEmail($_POST['txt_email_adm']);
        $adm->setlLogin($_POST['txt_login_adm']);
        $adm->setSenha($_POST['txt_password_adm']);
        $adm->insert();
        if($adm->getId()!=null){
            header('location:principal.php?link=11&msg=ok');
        }
    }
//Excluir Adm;   
$id = filter_input(INPUT_GET,'id');
$excluir = filter_input(INPUT_GET,'excluir');
if(isset($id)&& $excluir==1){
$adm = new Administrador();
$adm->setId($id);
$adm->delete();
header('location:principal.php?link=11&msg=ok');
}
    

//Alterar adm
if (isset($_POST['alterar'])){
    $adm = new Administrador();
    $adm->update($_POST['id'],$_POST['nome'],$_POST['email'],$_POST['login']);
    header('location:principal.php?link=11&msg=ok');
}

//Registrar sessão do Administrador;
if(isset($_POST['logado'])){
    $txt_login = isset($_POST['txt_login'])?$_POST['txt_login']:'';
    $txt_senha = isset($_POST['txt_senha'])?$_POST['txt_senha']:'';
$adm->efetuarLogin($txt_login, $txt_senha);
if(($adm->getId()==null)){
    echo $adm->getId(); 
    header('location:index.php?msg=Administrador ou senha incorretos');
    exit;
    }
    else if($adm->getId()>0){
        $_SESSION['logado'] = true;
        $_SESSION['id'] = $adm->getId();
        $_SESSION['nome_adm'] = $adm->getNome();
        $_SESSION['senha_adm'] = $adm->getSenha();
    
        header('location:principal.php?link=1');
    }
}
if(isset($_GET['sair'])){
    $_SESSION['logado'] = false;
    $_SESSION['id'] = null;
    $_SESSION['nome_adm'] = null;
    $_SESSION['senha_adm'] = null;

    header('location:index.php');
}
?>