<?php
require_once('../config.php');
    if(isset($_POST['btn_cadastrar'])){
        $cat = new Categoria();
        $cat->setCategoria($_POST['txt_categoria']);
        $cat->setCat_ativo(isset($_POST['cat_ativo'])?'1':'0');
        $cat->insert();
        if($cat->getIdCategoria()!=null){
            header('location:principal.php?link=3&msg=ok');
        }
    }
    
// excluir /delete categoria;
$id_categoria = filter_input(INPUT_GET,'id_categoria');
$excluir = filter_input(INPUT_GET,'excluir');
if(isset($id_categoria)&& $excluir==1){
    $categoria = new Categoria();
    $categoria->setIdCategoria($id_categoria);
    $categoria->delete();
    header('location:principal.php?link=3&msg=ok');
    }

//alterar o categoria;
if(isset($_POST['alterar'])){
    $cat = new Categoria();
    $cat->update($_POST['id_categoria'],$_POST['categoria'],isset($_POST['cat_ativo'])?'1':'0');
    header('location:principal.php?link=3&msg=ok');
    }

?>