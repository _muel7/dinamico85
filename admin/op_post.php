<?php
    require_once('../config.php');
    if(isset($_POST['btn_cadastrar'])){
        $foto = uploadImagem();
        $post = new Post();
        $post->setId_categoria($_POST['categoria']);
        $post->setTitulo($_POST['txt_titulo_post']);
        $post->setDescricao($_POST['txt_descricao']);
        $post->setImagem($foto[0]);
        $post->setVisitas($_POST['txt_visita']);
        $post->setDate($_POST['txt_data']);
        $post->setPost_ativo(isset($_POST['check_ativo'])?'1':'0');
        $post->insert();
        if($post->getIdPost()!=null){
            header('location:principal.php?link=4&msg=ok');
        }
    }
//Excluir Post;   
$id = filter_input(INPUT_GET,'id_post');
$excluir = filter_input(INPUT_GET,'excluir');
if(isset($id)&& $excluir==1){
$post = new Post();
$post->setIdPost($id);
$post->delete();
header('location:principal.php?link=5&msg=ok');
}

//Alterar Post
if (isset($_POST['alterar'])){
    $post = new Post();
    $post->update($_POST['id_post'],$_POST['id_categoria'],$_POST['titulo_post'],$_POST['descricao_post'],$_POST['img_post'],$_POST['visita'],$_POST['post_ativo']);
    header('location:principal.php?link=4&msg=ok');
}
?>  