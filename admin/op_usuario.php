<?php 
require_once('../config.php');
if(isset($_POST['cadastro_usuario'])){
    $usuario = new Usuario();
    $usuario->setNomeUsuario($_POST['nome']);
    $usuario->setEmailUsuario($_POST['email']);
    $usuario->setSenhaUsuario($_POST['senha']);
    $usuario->insert();
    if($usuario->getIdUsuario()!=null){
        header('location:../index.php?link=4&msg=ok');
    }
}
//Excluir Usuario;   
$id = filter_input(INPUT_GET,'id_usuario');
$excluir = filter_input(INPUT_GET,'excluir');
if(isset($id)&& $excluir==1){
$usuario = new Usuario();
$usuario->setIdUsuario($id);
$usuario->delete();
header('location:principal.php?link=12&msg=ok');
}

//Alterar Usuario;
if (isset($_POST['alterar'])){
$usuario = new Usuario();
$usuario->update($_POST['id_usuario'],$_POST['nome_usuario'],$_POST['email_usuario'],$_POST['senha_usuario']);
header('location:principal.php?link=12&msg=ok');
}

if(isset($_POST['logar'])){
$usuario = new Usuario(); 
$usuario->efetuarLogin($_POST['email'],$_POST['senha']);
    if($usuario->getIdUsuario()>0){
        $_SESSION['logar'] = true;
        $_SESSION['id_usuario'] = $usuario->getIdUsuario();
        $_SESSION['nome'] = $usuario->getNomeUsuario();
        $_SESSION['email'] = $usuario->getEmailUsuario();
        //$_SESSION['senha'] = $usuario->getSenhaUsuario();
            header('location:../index.php?link=2&msg=logado com succeso!');
    }
     
}
if(isset($_GET['sair'])){
    if($_GET['sair']){
    $_SESSION['logar'] = false;
    $_SESSION['id_usuario'] = null;
    $_SESSION['nome'] = null;
    $_SESSION['email'] = null;

    header('location:../index.php');
    }
}
?>
