<?php

//inicializar a sessão de usuário
if (!isset($_SESSION)){
    session_start();
}

//definindo padrão de Zona GMT (TimeZone) -3,00
date_default_timezone_set('America/Sao_Paulo');

//inicia carregamento das classes do projeto
spl_autoload_register(function($nome_classe){
    $nome_arquivo = "class".DIRECTORY_SEPARATOR.$nome_classe.".php";
    if(file_exists($nome_arquivo)){
        require_once($nome_arquivo);
    }
});

spl_autoload_register(function($nome_classe){
    $server_str = $_SERVER['REQUEST_URI'];
    //vai verificar a origem da chamada da classe(caso venha de arquivos da pasta admin apenas usa'class'  
    //porém se estiver vindo de outro caminho insere 'admin\class')
    $caminho = (strpos($server_str, "admin") !== false)?"class":"admin/class";
    $nome_arquivo = $caminho.DIRECTORY_SEPARATOR.$nome_classe.'.php';
    if(file_exists($nome_arquivo)){
        require_once($nome_arquivo);
    }
});

function uploadImagem(){
        $foto=$_FILES['foto'];

        if(!empty($foto['name'])){
            $largura = LARGURA_IMG;
            $altura = 425;
            $tamanho= 300000;
            $error = array();
            if(!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/",$foto['type'])){
                $error[1] = "Este arquivo não é uma imagem";
            }


            $dimensoes = getimagesize($foto['tmp_name']);
            if($dimensoes[0]>$largura){
                $error[2] = "A largura(".$dimensoes[0]."pixels) é maior que a suportada (".$largura." bytes)";
            }

            $dimensoes = getimagesize($foto['tmp_name']);
            if($dimensoes[1]>$altura){
                $error[3] = "A altura(".$dimensoes[1]."pixels) é maior que a suportada (".$altura." bytes)";
            }

            if($foto['size']>$tamanho){
                $error[4] = "O tamanho da imagem(".$foto[0]."bytes) é maior que a suportada (".$foto." bytes)";
            }

            //recuperar a extensao do arquivo;
            if(count($error)==0){
                preg_match("/\.(gif|bmp|png|jpg){1}$/i",$foto['name'],$ext);
                //gerar nome de arquivo para img;
                $nome_img = md5(uniqid(time())).$ext[0];
                $caminho_img = "foto/".$nome_img;
                move_uploaded_file($foto['tmp_name'],$caminho_img);
            }
        }
    $imagem_info = array();
    $imagem_info[0]=$nome_img;
    $imagem_info[1]=$error;
    return $imagem_info;
}
//Criar constantes do servidor de banco de dados
define ('IP_SERVER_DB', '127.0.0.1');
define ('HOSTNAME','ITQ0626030W10-1');
define ('NOME_BANCO','dinamico85db');
define ('USER_DB','root');
define ('PASS_DB','');
define ('LARGURA_IMG',640);
?>