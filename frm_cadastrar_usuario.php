<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Cadastrar Usuário</title>
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>
    <h1>Novo Usuário</h1>
    <form action="admin/op_usuario.php" method="post" enctype="multipart/form-data" name="cadastro_form">
        Nome:<br>
        <input type="text" name="nome" id="nome_usuario" required><br>
        Email:<br>
        <input type="text" name="email" id="email_usuario" required><br>
        Senha:<br>
        <input type="password" name="senha" id="senha_usuario" required><br>
        <br>
        <input type="submit" name="cadastro_usuario" value="Cadastrar usuario" class="btn-cadastrar">
        <span><?php echo (isset($_GET['msg']))?"Sucesso, usuário cadastrado!":'';?></span>
    </form>
    <br>

</body>
</html>